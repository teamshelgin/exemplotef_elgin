package application;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

public class TEF_CLASS implements driverTef {
	int CodigoOperacao;
    Object strJsonDados;
	IntByReference param;/////
	String DadosCaptura;
	int Acao;
	int Encerramento;
	
    public static driverTef driver;
    
	public TEF_CLASS() {
		String caminho = "C:\\Users\\bruno.cruz\\Desktop\\32 - Copy - Copia\\APITEFElgin.dll";
		driver = (driverTef) Native.loadLibrary(caminho, driverTef.class);
		System.out.println("Driver Carregado");
	}
	
	public int getCodigoOperacao() {
		return CodigoOperacao;
	}

	public void setCodigoOperacao(int codigoOperacao) {
		CodigoOperacao = codigoOperacao;
	}

	public Object getStrJsonDados() {
		return strJsonDados;
	}

	public void setStrJsonDados(Object strJsonDados) {
		this.strJsonDados = strJsonDados;
	}

	public IntByReference getParam() {
		return param;
	}

	public void setParam(IntByReference param) {
		this.param = param;
	}

	public String getDadosCaptura() {
		return DadosCaptura;
	}

	public void setDadosCaptura(String dadosCaptura) {
		DadosCaptura = dadosCaptura;
	}

	public int getAcao() {
		return Acao;
	}

	public void setAcao(int acao) {
		Acao = acao;
	}

	public int getEncerramento() {
		return Encerramento;
	}

	public void setEncerramento(int encerramento) {
		Encerramento = encerramento;
	}

    @Override
	public int ElginTEF_Autenticador() {
		return driver.ElginTEF_Autenticador();
	}

	@Override
	public int ElginTEF_IniciarOperacaoTEF() {
		return driver.ElginTEF_IniciarOperacaoTEF();
	}

	@Override
	public int ElginTEF_RealizarPagamentoTEF(int CodigoOperacao, Object strJsonDados, IntByReference param) {
		return driver.ElginTEF_RealizarPagamentoTEF(CodigoOperacao, strJsonDados, param);
	}

	@Override
	public String ElginTEF_RealizarPagamentoTEF2(int CodigoOperacao, String DadosCaptura) {
		return driver.ElginTEF_RealizarPagamentoTEF2(CodigoOperacao, DadosCaptura);
	}

	@Override
	public int ElginTEF_RealizarAdmTEF(int CodigoOperacao, Pointer jsonPointer, IntByReference param) {
		return driver.ElginTEF_RealizarAdmTEF(CodigoOperacao, jsonPointer, param);
	}

	@Override
	public String ElginTEF_RealizarAdmTEF2(int CodigoOperacao, String DadosCaptura) {
		return driver.ElginTEF_RealizarAdmTEF2(CodigoOperacao, DadosCaptura);
	}

	@Override
	public int ElginTEF_ConfirmarOperacaoTEF(int Acao) {
		return driver.ElginTEF_ConfirmarOperacaoTEF(Acao);
	}

	@Override
	public int ElginTEF_FinalizarOperacaoTEF(int Encerramento) {
		return driver.ElginTEF_FinalizarOperacaoTEF(Encerramento);
	}
}
