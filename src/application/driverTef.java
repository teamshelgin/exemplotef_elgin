package application;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

public interface driverTef extends Library {
	public int ElginTEF_Autenticador();
	public int ElginTEF_IniciarOperacaoTEF();
	public int ElginTEF_RealizarPagamentoTEF(int CodigoOperacao, Object strJsonDados,IntByReference param);
	public String ElginTEF_RealizarPagamentoTEF2(int CodigoOperacao, String DadosCaptura);
	public int ElginTEF_RealizarAdmTEF(int CodigoOperacao, Pointer jsonPointer, IntByReference param);
    public String ElginTEF_RealizarAdmTEF2(int CodigoOperacao, String DadosCaptura);
	public int ElginTEF_ConfirmarOperacaoTEF(int Acao);	
	public int ElginTEF_FinalizarOperacaoTEF(int Encerramento);
}
