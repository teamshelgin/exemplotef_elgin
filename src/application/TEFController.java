package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;

import Model.DriverTEFElgin;
import Model.ProcessaOperacao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.Pane;


// Elaborado por Edilson Julio 15/10/2019 - Elgin Software House 

public class TEFController implements Initializable {

	//COMPONENTES DE TELA
	@FXML
	private Label text, lblOne, lblTwo;;

	@FXML
	private Button one, two, three, four, five, six, seven, eight, nine, zero, clear, debito, credito, cancelamento, reimpressao, virgula;

	@FXML
	private JFXButton btnOne,  btnTwo,  btnTree;

	@FXML
	private JFXTextField txtOne;

	@FXML
	private JFXListView<String> lstOne;

	@FXML
	private TextField display;

	@FXML
	private TitledPane tp;

	@FXML
	private Pane panelOperacao;

	@FXML
	private JFXProgressBar progressBar;

	@FXML
	private TextArea txtLogs;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		display.setEditable(false);
		// CarregarAPI();
	}


	@FXML
	void operacao(ActionEvent event) {

		if(true) {
			ProcessaOperacao.INSTANCE.Start();
			return;
		}

	}

	public void OperacaoDebito() {
		ProcessaOperacao.INSTANCE.setValue(display.getText());
		ProcessaOperacao.INSTANCE.SetOperacao(DriverTEFElgin.PAGAMENTO_DEBITO);
		operacao(null);
		return;
	}

	public void OperacaoCredito() {
		ProcessaOperacao.INSTANCE.setValue(display.getText());
		ProcessaOperacao.INSTANCE.SetOperacao(DriverTEFElgin.PAGAMENTO_CREDITO);
		operacao(null);
		return;
	}

	public void OperacaoCancelamento() {
		ProcessaOperacao.INSTANCE.setValue(display.getText());
		ProcessaOperacao.INSTANCE.SetOperacao(DriverTEFElgin.OPERACAO_CANCELAMENTO);
		operacao(null);
		return;
	}

	public void OperacaoReimpressao() {
		ProcessaOperacao.INSTANCE.SetOperacao(DriverTEFElgin.OPERACAO_REIMPRESSAO);
		operacao(null);
		return;
	}

	public void OperacaoConfiguracao() {
		ProcessaOperacao.INSTANCE.SetOperacao(99);
		operacao(null);
		return;
	}
	
	@FXML
	void handleButtonAction(ActionEvent event) {
		if (event.getSource() == one) {
			display.setText(display.getText() + "1");
		} else if (event.getSource() == two)  {
			display.setText(display.getText() + "2");
		} else if (event.getSource() == three){
			display.setText(display.getText() + "3");
		} else if (event.getSource() == four) {
			display.setText(display.getText() + "4");
		} else if (event.getSource() == five) {
			display.setText(display.getText() + "5");
		} else if (event.getSource() == six)  {
			display.setText(display.getText() + "6");
		} else if (event.getSource() == seven){
			display.setText(display.getText() + "7");
		} else if (event.getSource() == eight){
			display.setText(display.getText() + "8");
		} else if (event.getSource() == nine) {
			display.setText(display.getText() + "9");
		} else if (event.getSource() == zero) {
			display.setText(display.getText() + "0");
		} else if (event.getSource() == clear){
			display.setText("");
		}else if (event.getSource() == virgula){
			display.setText(display.getText() + ".");
		}
	}

}
