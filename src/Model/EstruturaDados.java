package Model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EstruturaDados {
	
//	@Expose
	@SerializedName("CodigoRetorno")
	private String codigoRetorno;
	
	@Expose
	@SerializedName("SequenciaCaptura")
	private int sequenciaCaptura;
	
	@Expose
	@SerializedName("TipoFluxo")
	private int tipoFluxo;
	
	@Expose
	@SerializedName("InfoCaptura")
	private String infoCaptura;
	
	@Expose
	@SerializedName("ComponentesTela")
	private ArrayList<ComponentesTela>componentesTela;
		
	@Expose
	@SerializedName("AbortarFluxoCaptura")
	private boolean abortarFluxoCaptura;
	
	@Expose
	@SerializedName("FormatoInfoCaptura")
	private int formatoInfoCaptura;
	
	
	public int getSequenciaCaptura() {
		return sequenciaCaptura;
	}
	public void setSequenciaCaptura(int sequenciaCaptura) {
		this.sequenciaCaptura = sequenciaCaptura;
	}
	public int getTipoFluxo() {
		return tipoFluxo;
	}
	public void setTipoFluxo(int tipoFluxo) {
		this.tipoFluxo = tipoFluxo;
	}
	public String getInfoCaptura() {
		return infoCaptura;
	}
	public void setInfoCaptura(String infoCaptura) {
		this.infoCaptura = infoCaptura;
	}
	public int getFormatoInfoCaptura() {
		return formatoInfoCaptura;
	}
	public void setFormatoInfoCaptura(int formatoInfoCaptura) {
		this.formatoInfoCaptura = formatoInfoCaptura;
	}
	public boolean getAbortarFluxoCaptura() {
		return abortarFluxoCaptura;
	}
	public void setAbortarFluxoCaptura(boolean abortarFluxoCaptura) {
		this.abortarFluxoCaptura = abortarFluxoCaptura;
	}
	public ArrayList<ComponentesTela> getComponentesTelas() {
		return componentesTela;
	}
	public void setComponentesTelas(ArrayList<ComponentesTela> componentesTelas) {
		this.componentesTela = componentesTelas;
	}
	public String getCodigoRetorno() {
		return codigoRetorno;
	}
	public void setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}
	
}
