package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComponentesTela {

	@Expose
	@SerializedName("TipoVisor")
	private String tipoVisor;
	
	@Expose
	@SerializedName("TipoFluxo")
	private String tipoFluxo;
	
	@Expose
	@SerializedName("NomeComponenteTela")
	private String nomeComponenteTela;
	
	@Expose
	@SerializedName("ConteudoComponenteTela")
	private String conteudoComponenteTela;
	
	@Expose
	@SerializedName("CodigoComponenteTela")
	private int codigoComponenteTela;
	
	@Expose
	@SerializedName("FormatoDeCaptura")
	private int formatoDeCaptura;
	
	public String getTipoVisor() {
		return tipoVisor;
	}
	public void setTipoVisor(String tipoVisor) {
		this.tipoVisor = tipoVisor;
	}
	public String getTipoFluxo() {
		return tipoFluxo;
	}
	public void setTipoFluxo(String tipoFluxo) {
		this.tipoFluxo = tipoFluxo;
	}
	public String getNomeComponenteTela() {
		return nomeComponenteTela;
	}
	public void setNomeComponenteTela(String nomeComponenteTela) {
		this.nomeComponenteTela = nomeComponenteTela;
	}
	public String getConteudoComponenteTela() {
		return conteudoComponenteTela;
	}
	public void setConteudoComponenteTela(String conteudoComponenteTela) {
		this.conteudoComponenteTela = conteudoComponenteTela;
	}
	public int getCodigoComponenteTela() {
		return codigoComponenteTela;
	}
	public void setCodigoComponenteTela(int codigoComponenteTela) {
		this.codigoComponenteTela = codigoComponenteTela;
	}
	public int getFormatoDeCaptura() {
		return formatoDeCaptura;
	}
	public void setFormatoDeCaptura(int formatoDeCaptura) {
		this.formatoDeCaptura = formatoDeCaptura;
	}
	
	
}
