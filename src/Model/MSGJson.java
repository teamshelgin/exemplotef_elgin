package Model;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;


public class MSGJson extends EstruturaDados {
	
	class itens_da_lista {
		private int IdItem;
		public String ConteudoItem;
	}

	
	class lista_de_itens{
		private ArrayList<itens_da_lista> ArrayListBox;
	}
	
	
	private Gson json;
	private Pointer p;
	
	public static final int LENGTH = 4096;
	
	
	/**
	 * CONSTRUTOR CRIA A CLASSE COM OS VALORES PADR�O PARA INICIAR UMA OPERA��O
	 * @author Bruno Cruz 
	 * @return RETORNA UM OBJETO MSGjson
	 */
	public MSGJson(String valor) {
		this.setSequenciaCaptura(0);
		this.setTipoFluxo(DriverTEFElgin.TIPO_FLUXO_SOLICITACAO_CAPTURA);
		this.setInfoCaptura(valor);
		this.setComponentesTelas(null);
		this.setAbortarFluxoCaptura(false);
		this.setFormatoInfoCaptura(0);
		
		p = new Memory(LENGTH);
		p.setString(0, this.GetJson());
	}
	
	/**
	 * FUN��O USADA PARA RETORNAR O JSON COMO STRING DO OBJETO MSGJson
	 * @author Bruno Cruz 
	 * @return RETORNA UM OBJETO MSGjson
	 */
	public String GetJson() {
		json = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
		return json.toJson(this);
	}
	
	
	/**
	 * FUN��O DEVE SER USADA PARA ATUALIZAR OS DADOS DO OBJETO MSGJson USANDO O PONTEIRO ATUALIZADO PELA API
	 * @author Bruno Cruz 
	 * @return RETORNA UM BOOLEAN CASO A ATUALIZA��O SEJA BEM SUCEDIDA
	 */
	public boolean AtualizaJSon() {
		json = new Gson();
		MSGJson mAux = json.fromJson(this.p.getString(0), MSGJson.class);

		this.setSequenciaCaptura(mAux.getSequenciaCaptura());
		this.setTipoFluxo(mAux.getTipoFluxo());
		this.setInfoCaptura(mAux.getInfoCaptura());
		this.setComponentesTelas(mAux.getComponentesTelas());
		this.setAbortarFluxoCaptura(mAux.getAbortarFluxoCaptura());
		this.setFormatoInfoCaptura(mAux.getFormatoInfoCaptura());

		return true;
	}

	
	/**
	 * FUN��O RETORNA UM OBJETO POINTER PARA SER ENVIADO A FUN��O REALIZA PAGAMENTO TEF
	 * SEMPRE QUE ESSA FUN��O � CHAMADA ELA ATUALIZA AS INFORMA��ES DA CLASSE COM BASE NO PONTEIRO
	 * @author Bruno Cruz 
	 * @return RETORNA UM OBJETO POINTER
	 */
	public Pointer GetPointer() {
		p.setString(0, this.GetJson());
		return p;
	}
	
	public ArrayList<String> GetList(int index) {
		ArrayList<String> dados = new ArrayList<String>();
		System.out.println(this.getComponentesTelas().get(index).getConteudoComponenteTela());
		
		Gson g = new Gson();
		lista_de_itens l = g.fromJson(this.getComponentesTelas().get(index).getConteudoComponenteTela(), lista_de_itens.class);
		
		for (itens_da_lista item : l.ArrayListBox) {
			dados.add(item.ConteudoItem);
		}
		
		return dados;
	}
	
	
}
