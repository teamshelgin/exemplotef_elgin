package Model;

import com.sun.org.glassfish.gmbal.Description;

/**
 * @author bruno.cruz
 * {@link Description} Classe utilizada para mapear itens retornados da transa��o pela APITEFElgin
 */
public class DadoTransacao {
	private String chave;
	private String valor;
	
	public DadoTransacao() {
		
	}
	
	public DadoTransacao(String chave,String valor) {
		setChave(chave);
		setValor(valor);
	}
	
	public String getChave() {
		return chave;
	}
	public void setChave(String chave) {
		this.chave = chave;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
