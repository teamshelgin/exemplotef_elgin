package Model;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

public interface DriverTEFElgin extends Library{
	
//	CONSTANTES PARA TIPO FLUXO
	public static final int TIPO_FLUXO_SOLICITACAO_CAPTURA 	= 0;
	public static final int TIPO_FLUXO_PROSSEGUIR_CAPTURA 	= 1;
	public static final int TIPO_FLUXO_CANCELAR_CAPTURA 	= 2;
	public static final int TIPO_FLUXO_RETORNAR_CAPTURA 	= 3;
	public static final int TIPO_FLUXO_SEGUIR_FLUXO 		= 4;
	
//	OPERA합ES DE PAGAMENTO
	public static final int PAGAMENTO_DEBITO  = 1;
	public static final int PAGAMENTO_CREDITO = 2;
	public static final int OPERACAO_CANCELAMENTO = 12;
	public static final int OPERACAO_REIMPRESSAO = 128;

//	OP합ES PARA CONFIRMA플O DA TRANSA플O OU CANCELAMENTO
	public static final int CANCELAR_OPERACAO  = 0;
	public static final int CONFIRMAR_OPERACAO = 1;

//	OP합ES PARA FINALIZA플O DA OPERA플O
	public static final int FINALIZAR_TRANSACAO = 0;  //Automa豫o comercial continuar� executando.
	public static final int FINALIZAR_AUTOMACAO = 1;  //Automa豫o comercial est� sendo encerrada
	
	public static DriverTEFElgin INSTANCE = Native.loadLibrary("APITEFElgin.dll", DriverTEFElgin.class);

	public int ElginTEF_Autenticador();

	public int ElginTEF_IniciarOperacaoTEF();

	public int ElginTEF_RealizarPagamentoTEF(int CodigoOperacao, Pointer strJsonDados, IntByReference param);

	public String ElginTEF_RealizarPagamentoTEF2(int CodigoOperacao, String DadosCaptura);

	public int ElginTEF_RealizarAdmTEF(int CodigoOperacao, Pointer jsonPointer, IntByReference param);

	public String ElginTEF_RealizarAdmTEF2(int CodigoOperacao, String DadosCaptura);

	public int ElginTEF_ConfirmarOperacaoTEF(int Acao);	

	public int ElginTEF_FinalizarOperacaoTEF(int Encerramento);
	
	public int ElginTEF_RealizarConfiguracao(Pointer DadosCaptura, IntByReference tamDados);
	
	public int ElginTEF_RealizarCancelametoTEF(Pointer DadosCaptura, IntByReference tamDados);
}
