package Model;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import com.sun.jna.ptr.IntByReference;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public final class ProcessaOperacao implements Initializable {

	@FXML
	private JFXButton btnOne;

	@FXML
	private JFXButton btnTwo;

	@FXML
	private JFXButton btnTree;

	@FXML
	private Label lblOne;

	@FXML
	private Label lblTwo;

	@FXML
	private JFXTextField txtOne;

	@FXML
	private JFXListView<String> lstOne;

	@FXML
	private BorderPane panelOperacao;

	@FXML
	private JFXProgressBar progressBar;

	public static final ProcessaOperacao INSTANCE = new ProcessaOperacao();
	private static final Stage primaryStage = new Stage();
	private static Service<Void> tarefa;
	private int retorno = -1;
	private static int op = 0;
	private static MSGJson msg;
	private String valorTransacao = "";
	
	private boolean seguir = false;
	private Alert a;
	private TextArea area;

	public void Start() {
		try {
			msg = new MSGJson(valorTransacao);
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("src/View/Pagamento.fxml"));
			Scene scene = new Scene(loader.load());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Processamento");
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void IniciaEstadoEspera() {
		progressBar.setVisible(true);
	}

	private void FinalizaEstadoEspera() {
		progressBar.setVisible(false);
	}

	private void restartTarefa() {
		tarefa.restart();
	}

	public void IniciaOperacao() {
		IniciaEstadoEspera();

		if (op != 99) {
			if ((retorno = DriverTEFElgin.INSTANCE.ElginTEF_Autenticador()) != 0) {
				lblOne.setText("Erro na Autentica��o." + retorno);
				FinalizaEstadoEspera();
				return;
			}
			if ((retorno = DriverTEFElgin.INSTANCE.ElginTEF_IniciarOperacaoTEF()) != 0) {
				lblOne.setText("Erro ao Iniciar Opera��o. " + retorno);
				FinalizaEstadoEspera();
				return;
			} 
		}
		tarefa = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {

					@Override
					protected Void call(){
						IntByReference param = new IntByReference(MSGJson.LENGTH);

						if(op == 99) {
							retorno = DriverTEFElgin.INSTANCE.ElginTEF_RealizarConfiguracao(msg.GetPointer(), param);
						}else if(op > DriverTEFElgin.PAGAMENTO_CREDITO){
							retorno = DriverTEFElgin.INSTANCE.ElginTEF_RealizarAdmTEF(op, msg.GetPointer(), param);
						}else {
							retorno = DriverTEFElgin.INSTANCE.ElginTEF_RealizarPagamentoTEF(op, msg.GetPointer(), param);
						}
						System.err.println("Messagem retornada da api");

						//APOS O PROCESSAMENTO DA CHAMADA O OBJETO MSG DEVE SER ATUALIZADO.
						msg.AtualizaJSon();

						return null;
					}
				};
			}
			@Override
			protected void succeeded() {
				//ATUALIZA CAMPOS DA TELA
				ProcessaRetorno();
				if(msg.getSequenciaCaptura() != 99 && seguir) {
					seguir  = false;
					restartTarefa();
					return ;
				}else if(msg.getSequenciaCaptura() == 99) {
					if(retorno != 0) {
						Alert a = new Alert(AlertType.ERROR);
						a.setTitle("Erro!");
						a.setContentText("Erro ao realizar pagamento: "+ retorno);
						a.showAndWait();
						if(op != 99)
							DriverTEFElgin.INSTANCE.ElginTEF_ConfirmarOperacaoTEF(DriverTEFElgin.CANCELAR_OPERACAO);

					}else {
						if(op != 99) {
							if((retorno = DriverTEFElgin.INSTANCE.ElginTEF_ConfirmarOperacaoTEF(DriverTEFElgin.CONFIRMAR_OPERACAO)) != 0) {
								Alert a = new Alert(AlertType.ERROR);
								a.setTitle("Erro!");
								a.setContentText("Erro ao realizar Confirma��o: "+ retorno);
								a.showAndWait();
							}else if((retorno = DriverTEFElgin.INSTANCE.ElginTEF_FinalizarOperacaoTEF(DriverTEFElgin.FINALIZAR_TRANSACAO)) != 0) {
								Alert a = new Alert(AlertType.ERROR);
								a.setTitle("Erro!");
								a.setContentText("Erro ao finalizar transa��o: "+ retorno);
								a.showAndWait();
							}
						}
					}
					FinalizaEstadoEspera();
				}
			}
		};


		tarefa.setOnFailed(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				System.out.println(tarefa.getException());
				Alert a = new Alert(AlertType.ERROR);
				a.setTitle("Erro!");
				a.setContentText(tarefa.getException().toString());
				a.showAndWait();
				FinalizaEstadoEspera();
			}
		});

		tarefa.start();
	}

	@FXML
	public void ProcessaRetorno() {
		lblOne.setVisible(false);
		lblTwo.setVisible(false);

		btnOne.setVisible(false);
		btnTwo.setVisible(false);
		btnTree.setVisible(false);

		txtOne.setVisible(false);
		lstOne.setVisible(false);

		//VALIDAR RETORNO ANTES DE ALTERAR TELA
		if(msg.getComponentesTelas() == null || msg.getComponentesTelas().size() == 0) {
			Alert a = new Alert(AlertType.ERROR);
			a.setTitle("Erro");
			a.setContentText("NENHUMA INFORMA��O RETORNADA DA API!");
			a.showAndWait();
			return;
		}

		for (int i = 0; i < msg.getComponentesTelas().size(); i++) {
			ComponentesTela c = msg.getComponentesTelas().get(i);
			switch (c.getNomeComponenteTela().toUpperCase()) {

			case "LABEL":
				if(msg.getComponentesTelas().get(i).getTipoVisor() != null && msg.getComponentesTelas().get(i).getTipoVisor().equals("operador")) {
					if(c.getCodigoComponenteTela() < 2) {
						lblOne.setText(c.getConteudoComponenteTela());
						lblOne.setVisible(true);
					}else {
						lblTwo.setText(c.getConteudoComponenteTela());
						lblTwo.setVisible(true);
					}
				}
				break;

			case "BUTTON":
				switch (c.getConteudoComponenteTela().toUpperCase()) {

				case "PROSSEGUIR":
					btnOne.setVisible(true);
					btnOne.setText(c.getConteudoComponenteTela());
					break;

				case "CANCELAR":
					btnTwo.setVisible(true);
					btnTwo.setText(c.getConteudoComponenteTela());
					break;

				case "VOLTAR":
					btnTree.setVisible(true);
					btnTree.setText(c.getConteudoComponenteTela());
					break;
				}
				break;

			case "TEXTBOX":
				txtOne.setText(msg.getComponentesTelas().get(i).getConteudoComponenteTela());
				txtOne.setVisible(true);
				Platform.runLater(()->{
					txtOne.requestFocus();
				});
				break;

			case "LISTBOX":
				lstOne.getItems().clear();
				lstOne.setVisible(true);
				lstOne.getItems().addAll(msg.GetList(i));
				break;

			case "COMPROVANTELOJA":
				a = new Alert(AlertType.INFORMATION);
				a.setTitle("Comprovante Loja");
				a.setHeaderText("Comprovante da opera��o");

				area = new TextArea(c.getConteudoComponenteTela());
				area.setWrapText(true);
				area.setEditable(false);

				area.setPrefHeight(750); 
				area.setPrefWidth(400);

				a.getDialogPane().setContent(area);
				a.setResizable(true);
				a.showAndWait();
				break;

			case "COMPROVANTECLIENTE":
				a = new Alert(AlertType.INFORMATION);
				a.setTitle("Comprovante Cliente");
				a.setHeaderText("Comprovante da opera��o");

				area = new TextArea(c.getConteudoComponenteTela());
				area.setWrapText(true);
				area.setEditable(false);

				area.setPrefHeight(750); 
				area.setPrefWidth(400);

				a.getDialogPane().setContent(area);
				a.setResizable(true);
				a.showAndWait();
				break;

			case "DADOSTRANSACAO":
				a = new Alert(AlertType.INFORMATION);
				a.setTitle("Dados da Transa��o");
				a.setHeaderText("Dados da Transa��o");

				TableView<DadoTransacao> dadosTrans = new TableView();
				dadosTrans.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

				TableColumn<DadoTransacao, String> col1 = new TableColumn<DadoTransacao, String>("Chave");
				col1.setCellValueFactory(new PropertyValueFactory<>("chave"));
				TableColumn<DadoTransacao, String> col2 = new TableColumn<DadoTransacao, String>("Valor");
				col2.setCellValueFactory(new PropertyValueFactory<>("valor"));

				dadosTrans.getColumns().addAll(col1, col2);
				ObservableList<DadoTransacao> data = FXCollections.observableArrayList();

				String retApi[] = msg.getComponentesTelas().get(i).getConteudoComponenteTela().split(",");
				String aux[];
				DadoTransacao dados;

				for (String line : retApi) {

					dados = new DadoTransacao();
					aux = line.split(":");
					dados.setChave(aux[0]);
					dados.setValor(aux.length==1?"":aux[1]);

					data.add(dados);
				}

				dadosTrans.setItems(data);

				dadosTrans.setPrefHeight(550); 
				dadosTrans.setPrefWidth(400);

				a.getDialogPane().setContent(dadosTrans);
				a.setResizable(true);
				a.showAndWait();

				break;
			case "SEGUIRFLUXO":
				seguir = true;
				break;

			default:
				break;
			}
		}
	}

	public void OperacaoBotaoOK() {
		if(txtOne.isVisible()) {
			System.out.println(txtOne.getText());
			if(txtOne.getText().equals("")) {
				Alert a = new Alert(AlertType.ERROR);
				a.setTitle("Erro");
				a.setContentText("Preencha todos os campos!");
				a.showAndWait();
				txtOne.requestFocus();
			}else {
				msg.setInfoCaptura(txtOne.getText());
				msg.setTipoFluxo(DriverTEFElgin.TIPO_FLUXO_PROSSEGUIR_CAPTURA);
				restartTarefa();
				return;
			}
		}else if(lstOne.isVisible()) {
			if(lstOne.getSelectionModel().getSelectedIndex() == -1) {
				Alert a = new Alert(AlertType.ERROR);
				a.setTitle("Erro");
				a.setContentText("Selecione uma op��o!");
				a.showAndWait();
			}else {
				msg.setInfoCaptura(String.valueOf(lstOne.getSelectionModel().getSelectedIndex() +1));
				msg.setTipoFluxo(DriverTEFElgin.TIPO_FLUXO_PROSSEGUIR_CAPTURA);
				restartTarefa();
				return;
			}
		}else {
			msg.setTipoFluxo(DriverTEFElgin.TIPO_FLUXO_PROSSEGUIR_CAPTURA);
			restartTarefa();
			return;
		}
	}

	public void OperacaoBotaoVoltar() {
		msg.setTipoFluxo(DriverTEFElgin.TIPO_FLUXO_RETORNAR_CAPTURA);
		restartTarefa();
	}

	public void OperacaoBotaoCancelar() {

		msg.setTipoFluxo(DriverTEFElgin.TIPO_FLUXO_CANCELAR_CAPTURA);
		restartTarefa();
	}

	public void SetOperacao(int operacao) {
		op = operacao;
	}
	
	public void setValue(String valor) {
		valorTransacao = valor;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Platform.runLater(() ->{
			IniciaOperacao();
		});

	}

}
