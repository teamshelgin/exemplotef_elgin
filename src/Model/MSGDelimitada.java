package Model;

import java.util.ArrayList;
import java.util.Arrays;

public class MSGDelimitada extends EstruturaDados{

	private String value;
	
	//SEPARADORES PARA MSG DELIMITADA
	private final String SEPARADOR_NIVEL_1 = "\\|\\|"; 		// Separador de 1� N�vel
	private final String SEPARADOR_NIVEL_2 = "\\(\\("; 		// Separador de 2� N�vel
	private final String SEPARADOR_NIVEL_3 = "(\\[\\[)"; 	// Separador de 3� N�vel
	private final String SEPARADOR_NIVEL_4 = "(\\{\\{)"; 	// Separador de 4� N�vel
	
	//VARIAVEIS PARA OS COMPONENTES DA MSG
	private ArrayList<String> respostas1;		//USADO PARA ARMAZENAS OS RETORNOS DO PRIMEIRO NIVEL DE DADOS
	private ArrayList<String> respostas2;		//USADO PARA ARMAZENAS OS RETORNOS DO SEGUNDO NIVEL DE DADOS(COMPONENTES TELA)
	private ArrayList<String> respostas3;
	private ArrayList<String> itemsDoArray;
	
	private ArrayList<ComponentesTela> componentes = new ArrayList<ComponentesTela>();
		
	
	/*
	 * Realiza o split da MSG de resposta e grava nas variaveis adequadas
	 * REALIZA O SPLIT DA MSG DE RESPOSTA DA API E GRAVA NAS VARIAVEIS
	 */
	private void DecodificaMSG(String dados) throws Exception {
		
		if(value.equals(""))
			throw new Exception("Resposta API Vazia!");
		
		//PRIMEIRO DECODE
		respostas1 = new ArrayList<String> (Arrays.asList(value.split(SEPARADOR_NIVEL_1)));
		
		if(respostas1.isEmpty())
			throw new Exception("Resposta API INV�LIDA!");
		
		if(respostas1.size() == 1)
			throw new Exception("Resposta API INV�LIDA!");
		
		//SEGUNDO DECODE
		respostas2 = new ArrayList<String> (Arrays.asList(respostas1.get(4).split(SEPARADOR_NIVEL_2)));
		
		if(respostas2.isEmpty())
			throw new Exception("Resposta referente ao componentes tela INV�LIDA!");
		
		this.setCodigoRetorno(respostas1.get(0));
		this.setSequenciaCaptura(Integer.parseInt(respostas1.get(1)));
		this.setTipoFluxo(Integer.parseInt(respostas1.get(2)));
		this.setInfoCaptura(respostas1.get(3));
		
		//COLETANTO DADOS DOS COMPONENTES TELA
		for (String string : respostas2) {
			respostas3 = new ArrayList<String> (Arrays.asList(string.split(SEPARADOR_NIVEL_3))); // TRATA CADA COMPONENTE DE TELA RETORNANDO
			
			ComponentesTela t = new ComponentesTela();
			t.setCodigoComponenteTela(Integer.parseInt(respostas3.get(0)));
			t.setNomeComponenteTela(respostas3.get(1));
			
			setItemsDoArray(new ArrayList<String> (Arrays.asList(respostas3.get(2).split(SEPARADOR_NIVEL_4))));
			
			t.setConteudoComponenteTela(respostas3.get(2));
			
			t.setFormatoDeCaptura(Integer.parseInt(respostas3.get(3)));
			componentes.add(t);
		}
		
		this.setComponentesTelas(componentes);
		this.setAbortarFluxoCaptura(Boolean.parseBoolean(respostas1.get(5)));
		this.setFormatoInfoCaptura(Integer.parseInt(respostas1.get(6)));
		
	}

	public Boolean MSGCreate(String val) {
		try {
			DecodificaMSG(val);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String MSGToSend() {
		String dados = String.format("%s||%s||%s||%s||%s||%s", 
				this.getSequenciaCaptura(), 
				this.getTipoFluxo(), 
				this.getInfoCaptura(), 
				"",
				this.getAbortarFluxoCaptura(),
				this.getFormatoInfoCaptura());
		
		return dados;
	}

	@Override
	public String toString() {
		return value.toString();
	}

	
	/*
	 * FUN��O USADA PARA CAPTURAR OS ITENS DE UM COMPONENTE TELA DO TIPO ARRAYLIST
	 */
	public ArrayList<String> getItemsDoArray() {
		return itemsDoArray;
	}

	private void setItemsDoArray(ArrayList<String> itemsDoArray) {
		this.itemsDoArray = itemsDoArray;
	}
}
